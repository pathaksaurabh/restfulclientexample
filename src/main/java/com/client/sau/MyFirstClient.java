package com.client.sau;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class MyFirstClient {
	
public static void aa(String[] args) {
	Client client = Client.create();
	WebResource webResource = client.resource("http://localhost:8080/com.sau.rest/hello/param");
	webResource.accept("text");
	ClientResponse response = webResource.get(ClientResponse.class);
	System.out.println(response.getEntity(String.class));
	System.out.println("end");
}
}
